<?php	 	 
$donde_estoy="compra";
require_once('Connections/db1.php');
require_once('includes/functions.inc.php');
require_once('secure.php');
require_once('lan/idiomas.php');
require_once('includes/Control.php');



$cot = ConsultaCotizacion($db1,$_GET['id_cot']);

$query_pasajeros = "
	SELECT *, 
			c.id_pais as id_pais,
			DATE_FORMAT(c.cp_fecserv, '%d-%m-%Y') as cp_fecserv1, DAYOFMONTH(c.cp_fecserv) as dia, MONTH(c.cp_fecserv) as mes, YEAR(c.cp_fecserv) as ano
	FROM cotpas c
	LEFT JOIN pais p ON c.id_pais = p.id_pais
	LEFT JOIN ciudad i ON c.id_ciudad = i.id_ciudad
	WHERE id_cot = ".$_GET['id_cot']." AND c.cp_estado = 0";
$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
$totalRows_pasajeros = $pasajeros->RecordCount();

$query_ciudad = "SELECT c.*
	FROM trans as t
	INNER JOIN ciudad as c ON t.id_ciudad = c.id_ciudad
	WHERE t.id_area = 1 and t.id_mon = 1 and t.tra_estado = 0
	GROUP BY t.id_ciudad ORDER BY ciu_nombre";
$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());




include("htmlhead.php");
?>
<script type="text/javascript" src="js/paso3.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		var cantPax = $("#cantPax").val();
		var id_cot = $("#id_cot").val();
		var id_cotpas="";
		var x=0;
	
		for(x=1;x<=cantPax;x++){
			id_cotpas=$("#p_"+x).val();
			
			ajaxServiciosPax(id_cotpas, id_cot);
		}
		$("#slider-primero").show(1500);
		seg(7,id_cot);
	});
</script>
<body>
	<input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
	<input type="hidden" id="cantPax" name="cantPax" value="<? echo $pasajeros->RecordCount();?>" />
	<input type="hidden" id="id_cont" name="id_cont" value="<? echo $cot->Fields("id_cont");?>" />
  <div id="main">	
	<div id="site_content">
		<?php	 	 
			include("head.php");
		?>
		
		  <div id="content">
			  <table width="100%">
				<tr>
					<td width="100%" align="center">
						<h1>
							N&uacute;mero de Compra <? echo $_GET['id_cot'];?>
						</h1>				
					</td>
				</tr>
				<tr>
					<td width="100%" align="center">
						<h3>
							Total   $<? echo $cot->Fields("cot_valor");?>
						</h3>				
					</td>
				</tr>				
			  </table>		  
				<? $z=1;
				while (!$pasajeros->EOF) {?>
					<input type="hidden" id="c_<?=$pasajeros->Fields('id_cotpas')?>" name="c" value="<? echo $z;?>" />
					<input type="hidden" id="p_<? echo $z;?>" value="<?=$pasajeros->Fields('id_cotpas')?>" />
					<div id="pax_'<?=$z?>'" class="content">
						<h3>
							Pasajero <?=$z?> 
							<h4>
								<? echo $pasajeros->Fields('cp_nombres')." ". $pasajeros->Fields('cp_apellidos')."   (".$pasajeros->Fields('pai_nombre').")";?>
							</h4>
						</h3>
					</div>
					<table width="100%" class="programa">
						<tr>
							<td>

								<!--Servicios Agregados al Pax-->
								<div style="display:none" id="servicios_agregados_<?=$pasajeros->Fields('id_cotpas')?>">
									<div id="pax_'<?=$z?>'" class="content">
										<h4>
											Servicios Agregados
										</h4>
									</div>								
									<table width="100%" class="tabla_datos" id="tab_servicios_agregados_<?=$pasajeros->Fields('id_cotpas')?>">
										<input type="hidden" id="cantServ_<?=$pasajeros->Fields('id_cotpas')?>" value="0">
										<tr valign="baseline">
											<th width="290"><? echo $serv;?></th>
											<th width="100"><? echo $fechaserv;?></th>
											<th width="111" align="right"><?= $valor ?></th>
										</tr>
									</table>
									<table width="100%" class="tabla_datos" >
										<tr valign="baseline">
											<th width="290">&nbsp;</th>
											<th width="100" align="right">Total</th>
											<th width="111" align="right">
												<div id="tab_total_pax_<?=$pasajeros->Fields('id_cotpas')?>"></div>
											</th>
										</tr>
									</table>									
								</div>
								<div class="content_item">
									&nbsp;
								</div>
								<br style="clear:both"/>								
							</td>
						</tr>
					</table>
				<?php	 	 
					$z++;
					$pasajeros->MoveNext();
				} ?>
		</div><!--close content-->	  
			  <div class="sidebar_container">  		  
				<div id="slider-primero" class="sidebar" style="display:none;">
				  <div class="sidebar_item">
					<h1>Quieres editar esta compra?</h1>
					<p>Si deseas editar esta compra, puedes hacerlo
						<a href="compra_serv.php?id_cot=<?=$_GET["id_cot"]?>"> aqu&iacute; </a>
					</p>
				  </div><!--close sidebar-->    
				</div>
			  </div><!--close sidebar_container-->  
	</div><!--close site_content-->	
	<?php	 	 
		include("footer.php");
	?>
  </div><!--close main-->	
</body>
</html>
