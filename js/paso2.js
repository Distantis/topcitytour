function puedeComprarServicio(anio, mes, dia, horario){

	mes = mes-1;
	var fServicio = new Date(anio, mes, dia);
	fServicio.setHours(0,0,0,0);

	var hoyAhora= new Date();
	var diaSemanaHoy= hoyAhora.getDay();
	if((diaSemanaHoy!=6)&&(diaSemanaHoy!=0)){
		
		var hoy= new Date();
		hoy.setHours(0,0,0,0);

		var maniana= new Date();
		maniana.setDate(maniana.getDate()+1);
		maniana.setHours(0,0,0,0);

		var hoyLimite17Hrs= new Date();
		hoyLimite17Hrs.setHours(17,0,0,0);
	
		if(fServicio.valueOf()==maniana.valueOf()){
			if(hoyAhora>hoyLimite17Hrs){
				alert("Usted puede comprar servicios para mañana hasta las 17:00 hrs.\nSi aun desea comprar este servicio, por favor comuniquese con\n   'Juan Gaete'\n   al fono 789456123");
				return false;
			}
		}else if(fServicio.valueOf()==hoy.valueOf()){
			var hora=horario.split(":");
			var horaHorario=parseInt(hora);
			var horaAhora=parseInt(hoyAhora.getHours());

			if((horaHorario-horaAhora)<3){
				alert("Usted puede comprar servicios para hoy dentro de menos de 3 horas.\nSi aun desea comprar este servicio, por favor comuniquese con\n   'Juan Gaete'\n   al fono 789456123");				
				return false;
			}
		}
	}else{
		var diaMartes= new Date();
		if(diaSemanaHoy==6)
			diaMartes.setDate(diaMartes.getDate()+3);

		if(diaSemanaHoy==0)
			diaMartes.setDate(diaMartes.getDate()+2);

		diaMartes.setHours(0,0,0,0);

		if(fServicio.valueOf()==diaMartes.valueOf()){
				alert("Usted puede comprar servicios durante el fin de semana para antes del martes.\nSi aun desea comprar este servicio, por favor comuniquese con\n   'Juan Gaete'\n   al fono 789456123");
				return false;			
		}

	}

	return true;
}

function ajaxServicios(paxNum, conti, ciud, fec){
	$.ajax({
			type: 'POST',
			url: 'AjaxagregaServicios.php',
			data: { op: 6, cont: conti, ciu: ciud, fech: fec},
			dataType: 'html',
			success:function(result){
				var html='';
				html=result;
				$("#id_ttagrupa_"+paxNum).html(html);
			},
			error:function(){
				alert("Error!!")
			}
	});			
}

function ajaxAgregaServicio(id_transi, id_cotpasi, cs_fecpedi, cs_numtransi, obsi, id_coti){
	$("#spinner").show();
	$.ajax({
			type: 'POST',
			url: 'AjaxagregaServicios.php',
			data: { op: 2, 
					id_trans: id_transi, 
					id_cotpas: id_cotpasi, 
					cs_fecped: cs_fecpedi, 
					cs_numtrans: cs_numtransi, 
					obs: obsi, 
					id_cot: id_coti
				  },
			dataType: 'xml',
			asynx: false,
			success:function(result){
					var htmlSalida="";
					var cantServ=0;
					var id_cotser=0;
					var id_cotpas=0;
					var total_pax=0;
					if ($(result).find('servicios').first().find('servicio').length>0 ) {
						$(result).find('servicios').first().find('servicio').each(
							function(i){
								htmlSalida="";
								id_cotser=$(this).find("id_cotser").first().text();
								id_cotpas=$(this).find("id_cotpas").first().text();
								cantServ=parseInt($("#cantServ_"+id_cotpas).val())+1;
								document.getElementById("cantServ_"+id_cotpas).value=cantServ;												
								if($("#valor_ser_"+id_cotser).length>0){
									$("#valor_ser_"+id_cotser).html($(this).find("cs_valor").first().text());
								}else{
									htmlSalida+="<tr id='tr_"+id_cotser+"'>";
									//htmlSalida+="<td>"+cantServ+"</td>";
									htmlSalida+="<td>"+$(this).find("tra_nombre").first().text();
									if($(this).find("cs_valor").first().text()!=''){
										htmlSalida+="<br><font color='red' size='1'>"+$(this).find("cs_obs").first().text()+"</font>";
									}									
									htmlSalida+="</td>";
									htmlSalida+="<td>"+$(this).find("cs_fecped").first().text()+"</td>";
									//htmlSalida+="<td>"+$(this).find("cs_numtrans").first().text()+"</td>";
									//htmlSalida+="<td>Confirmado</td>";
									htmlSalida+="<td align='right' id='valor_ser_"+id_cotser+"'>"+$(this).find("cs_valor").first().text()+"</td>";
									htmlSalida+="<td align='right'><a onclick='javascript:ajaxElminaServicio("+id_cotser+","+id_cotpas+")'><img src='images/Delete.png' width='10' height='10' /></a></td>";
									htmlSalida+="</tr>";
									$("#tab_servicios_agregados_"+id_cotpas).append(htmlSalida);
									$("#servicios_agregados_"+id_cotpas).show('normal');
								}
								total_pax=parseFloat($(this).find("totalpax").first().text());
								$("#tab_total_pax_"+id_cotpas).html(total_pax);
								validaParaCheckout();
							}  
						);
					}
				}
	});	
	$("#spinner").hide();	
}

function agregaServicio(id_cotpasi){
	

	var numPax=$("#c_"+id_cotpasi).val();
	var id_transi = $("#id_ttagrupa_"+numPax).val();
	var cs_fec = $("#datepicker_"+numPax).val();
	cs_fecc = cs_fec.split('-');
	var cs_fecpedi = cs_fecc[2]+'-'+cs_fecc[1]+'-'+cs_fecc[0];
	var cs_numtransi = $("#txt_numtrans_"+id_cotpasi).val();
	var obsi = $("#txt_obs_"+id_cotpasi).val();
	var id_coti = $("#id_cot").val();
	var horario = $("#horarios_"+numPax+" option:selected").text();

	if(puedeComprarServicio(cs_fecc[2], cs_fecc[1], cs_fecc[0], horario)){
		if($("#horarios_"+numPax).val()!="0")
			obsi="HORARIO SERVICIO: "+horario+" hrs. -- "+obsi;

		ajaxAgregaServicio(id_transi, id_cotpasi, cs_fecpedi, cs_numtransi, obsi, id_coti);
	
		$("#slider-segundo").show(1500);
	}
}

function ajaxElminaServicio(id_cotseri,id_cotpasi){
	var id_coti = $("#id_cot").val();
	var cantServ=0;
	$.ajax({
			type: 'POST',
			url: 'AjaxagregaServicios.php',
			data: { op: 7, id_cotser: id_cotseri, id_cot: id_coti, id_cotpas: id_cotpasi},
			dataType: 'xml',
			success:function(result){
				if ($(result).find('servicios').first().find('servicio').length>0 ) {
					$(result).find('servicios').first().find('servicio').each(
							function(i){
								if($(this).find("status").first().text()=="1"){
									cantServ=parseInt($("#cantServ_"+id_cotpasi).val())-1;
									document.getElementById("cantServ_"+id_cotpasi).value=cantServ;									
									$("#tr_"+id_cotseri).remove();
									$("#tab_total_pax_"+id_cotpasi).html(parseFloat($(this).find("total").first().text()));
								}
							}  
					);
				}
				validaParaCheckout();
			},
			error:function(){
				alert("Error!!")
			}
	});	
}

function ChangeCSSToTextBox(textboxId){
	$(textboxId).css("background", "white" );
	$(textboxId).css("border", "1px solid #ffa853" );
	$(textboxId).css("border-radius", "5px" );
	$(textboxId).css("box-shadow", "0 0 5px 3px #ffa853" );
	$(textboxId).css("color", "#666" );
	$(textboxId).css("float", "left" );
	$(textboxId).css("padding", "5px 10px" );
	$(textboxId).css("outline", "none" );
	return true;
}

function validaParaCheckout(){
	var cantPax = $("#cantPax").val();
	var id_cotpas="";
	var x=0;
	var flag=true;
	
	for(x=1;x<=cantPax;x++){
		id_cotpas=$("#p_"+x).val();
		if(parseInt($("#cantServ_"+id_cotpas).val())<=0){
			flag=false;
		}
	}

	if(flag){
		$("#slider-segundo").show(1500);
		$("#slider-tercero").show(1500);
		$("#tabcheckout").show(1500);
	}else{
		$("#slider-tercero").hide(1500);
		$("#tabcheckout").hide(1500);	
			
	}
	
}

function toCheckout(){
	var id_coti = $("#id_cot").val();
	window.location='checkout.php?id_cot='+id_coti;
	
}

function cargaHorarios(id,cual){
	$.ajax({
			type: 'POST',
			url: 'AjaxagregaServicios.php',
			data: { op: 12, id_trans: id},
			dataType: 'html',
			success:function(result){
				var html='';
				html=result;
				$("#horarios_"+cual).html(html);
			},
			error:function(){
				alert("Error!!")
			}
	});	

	$.ajax({
			type: 'POST',
			url: 'AjaxagregaServicios.php',
			data: { op: 14, id_trans: id},
			dataType: 'html',
			success:function(result){
				var html='';
				html=result;
				$("#desc_"+cual).html(html);
			},
			error:function(){
				alert("Error!!")
			}
	});		
}

function ajaxServiciosPax(id_cotpasi, id_coti){
	$("#spinner").show();
	$.ajax({
			type: 'POST',
			url: 'AjaxagregaServicios.php',
			data: { op: 8, 
					id_cotpas: id_cotpasi, 
					id_cot: id_coti
				  },
			dataType: 'xml',
			asynx: false,
			success:function(result){
					var htmlSalida="";
					var cantServ=0;
					var id_cotser=0;
					var id_cotpas=0;
					var total_pax=0;
					if ($(result).find('servicios').first().find('servicio').length>0 ) {
						$(result).find('servicios').first().find('servicio').each(
							function(i){
								htmlSalida="";
								id_cotser=$(this).find("id_cotser").first().text();
								id_cotpas=$(this).find("id_cotpas").first().text();
								cantServ=parseInt($("#cantServ_"+id_cotpas).val())+1;
								document.getElementById("cantServ_"+id_cotpas).value=cantServ;												

									htmlSalida+="<tr id='tr_"+id_cotser+"'>";
									htmlSalida+="<td>"+$(this).find("tra_nombre").first().text();
									if($(this).find("cs_valor").first().text()!=''){
										htmlSalida+="<br><font color='red' size='1'>"+$(this).find("cs_obs").first().text()+"</font>";
									}									
									htmlSalida+="</td>";
									htmlSalida+="<td>"+$(this).find("cs_fecped").first().text()+"</td>";
									htmlSalida+="<td align='right' id='valor_ser_"+id_cotser+"'>"+$(this).find("cs_valor").first().text()+"</td>";
									htmlSalida+="<td align='right'><a onclick='javascript:ajaxElminaServicio("+id_cotser+","+id_cotpas+")'><img src='images/Delete.png' width='10' height='10' /></a></td>";									
									htmlSalida+="</tr>";
									$("#tab_servicios_agregados_"+id_cotpas).append(htmlSalida);
									$("#servicios_agregados_"+id_cotpas).show('normal');
								
								total_pax=parseFloat($(this).find("totalpax").first().text());
								$("#tab_total_pax_"+id_cotpas).html(total_pax);
								validaParaCheckout();
							}  
						);
					}
				}
	});	
	$("#spinner").hide();	
}