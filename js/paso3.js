function ajaxServiciosPax(id_cotpasi, id_coti){
	$("#spinner").show();
	$.ajax({
			type: 'POST',
			url: 'AjaxagregaServicios.php',
			data: { op: 8, 
					id_cotpas: id_cotpasi, 
					id_cot: id_coti
				  },
			dataType: 'xml',
			asynx: false,
			success:function(result){
					var htmlSalida="";
					var cantServ=0;
					var id_cotser=0;
					var id_cotpas=0;
					var total_pax=0;
					if ($(result).find('servicios').first().find('servicio').length>0 ) {
						$(result).find('servicios').first().find('servicio').each(
							function(i){
								htmlSalida="";
								id_cotser=$(this).find("id_cotser").first().text();
								id_cotpas=$(this).find("id_cotpas").first().text();
								cantServ=parseInt($("#cantServ_"+id_cotpas).val())+1;
								document.getElementById("cantServ_"+id_cotpas).value=cantServ;												

									htmlSalida+="<tr id='tr_"+id_cotser+"'>";
									htmlSalida+="<td>"+$(this).find("tra_nombre").first().text();
									if($(this).find("cs_valor").first().text()!=''){
										htmlSalida+="<br><font color='red' size='1'>"+$(this).find("cs_obs").first().text()+"</font>";
									}									
									htmlSalida+="</td>";
									htmlSalida+="<td>"+$(this).find("cs_fecped").first().text()+"</td>";
									htmlSalida+="<td align='right' id='valor_ser_"+id_cotser+"'>"+$(this).find("cs_valor").first().text()+"</td>";
									htmlSalida+="</tr>";
									$("#tab_servicios_agregados_"+id_cotpas).append(htmlSalida);
									$("#servicios_agregados_"+id_cotpas).show('normal');
								
								total_pax=parseFloat($(this).find("totalpax").first().text());
								$("#tab_total_pax_"+id_cotpas).html(total_pax);
							}  
						);
					}
				}
	});	
	$("#spinner").hide();	
}

function ajaxAnulaServicio(cot){
	if(!confirm("De verdad quieres anular esta reserva?"))
		return;

	$.ajax({
			type: 'POST',
			url: 'AjaxagregaServicios.php',
			data: { op: 13, 
					id_cot: cot
				  },
			dataType: 'html',
			asynx: false,
			success:function(result){
				if(parseInt(result)<2){
					alert("Error al Anular!");
				}else{
					$("#estado_"+cot).html("<font color='red'><b>Anulado!</b></font>");
					$("#anular_"+cot).html("&nbsp;");
				}


			}
	});	
}
