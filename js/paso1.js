function ajaxCantPax(desde, hasta){
	desde=desde+1;
	$.ajax({
			type: 'POST',
			url: 'AjaxagregaDatosPax.php?i='+desde+"&id_cotpas=0",
			data: {},
			async: false,
			success:function(result){
				var html='';
				html=result;
				$("#pax").append(html);
				$("#slider-segundo").show(1500);
				$("#slider-tercero").hide(1500);
				$("#tabinserta").hide(1500);
				if(desde<hasta)
					ajaxCantPax(desde, hasta);
					
			},
			error:function(){
				alert("Error!!")
			}
	});			
}

function ajaxeliminaPaxPreload(id_coti, id_cotpasi){
	$.ajax({
			type: 'POST',
			url: 'AjaxeliminaDatosPax.php',
			data: {id_cot: id_coti, id_cotpas: id_cotpasi},
			async: false,
			success:function(result){
				if(result=="0")
					alert("No se pudo eliminar el pasajero desde la base de datos");
			},
			error:function(){
				alert("Error!!")
			}
	});			
}


function ajaxPaxPreLoad(numPax,idPax){
	$.ajax({
			type: 'POST',
			url: 'AjaxagregaDatosPax.php?i='+numPax+"&id_cotpas="+idPax,
			data: {},
			async: false,
			success:function(result){
				var html='';
				html=result;
				$("#pax").append(html);
				$("#slider-segundo").show(1500);
				$("#slider-tercero").hide(1500);
				$("#tabinserta").hide(1500);
			},
			error:function(){
				alert("Error!!")
			}
	});			
}

function modifCantPax(){
	var numPas=parseInt($("#numpas").val());

	var actualNumPas=parseInt($("#cantPax").val());
	var id_cotpasi=0;
	var id_coti=0;
	if(actualNumPas<numPas){
		$("#spinner").show();
		ajaxCantPax(actualNumPas, numPas);
		$("#spinner").hide();
	}else if(actualNumPas>numPas){
		if (numPas==0){
			$("#slider-segundo").hide(1500);
			$("#slider-tercero").hide(1500);
			$("#tabinserta").hide(1500);
		}
		for(x=numPas+1;x<=actualNumPas;x++){
			id_cotpasi=$("#id_cotpas_"+x).val();
			id_coti=$("#id_cot").val();
			if(id_cotpasi!="0")
				ajaxeliminaPaxPreload(id_coti, id_cotpasi);

			$("#pax_"+x).remove();
		}
	}
	document.getElementById("cantPax").value=numPas;
	if (numPas==0){
		$("#slider-segundo").hide(1500);
		$("#slider-tercero").hide(1500);
		$("#tabinserta").hide(1500);
	}
	
}


function ChangeCSSToTextBox(textboxId){
	$(textboxId).css("background", "white" );
	$(textboxId).css("border", "1px solid #ffa853" );
	$(textboxId).css("border-radius", "5px" );
	$(textboxId).css("box-shadow", "0 0 5px 3px #ffa853" );
	$(textboxId).css("color", "#666" );
	$(textboxId).css("float", "left" );
	$(textboxId).css("padding", "5px 10px" );
	$(textboxId).css("outline", "none" );
	return true;
}

function ValidarDatos(){

	var numPas=parseInt($("#numpas").val());
	var mensaje="Faltan datos de los PAX:\n";
	var cont=0;
	for(x=1;x<=numPas;x++){
		mensaje=mensaje+"\n";
		if($("#txt_nombres_"+x).val()==""){
			ChangeCSSToTextBox("#txt_nombres_"+x);
			mensaje=mensaje+"- <?php echo $serv_trans_nombre; ?> "+x+"\n";
			cont++;
		}
		if($("#txt_apellidos_"+x).val()==""){
			ChangeCSSToTextBox("#txt_apellidos_"+x);
			mensaje=mensaje+"- <?php echo $serv_trans_apellido; ?> "+x+"\n";
			cont++;
		}					
		if($("#id_pais_"+x).val()==""){
			ChangeCSSToTextBox("#id_pais_"+x);
			mensaje=mensaje+"- <?php echo $pais_p_js; ?> del Pasajero "+x+"\n";
			cont++;
		}
	}
	
	if(cont>0){
		alert(mensaje);
	}else{
		$("#form1").submit();
	}
}

function ValidarDatos2(){

	var numPas=parseInt($("#numpas").val());
	var cont=0;
	for(x=1;x<=numPas;x++){
		if($("#txt_nombres_"+x).val()==""){
			cont++;
		}
		if($("#txt_apellidos_"+x).val()==""){
			cont++;
		}					
		if($("#id_pais_"+x).val()==""){
			cont++;
		}
	}
	
	if(cont==0&&numPas>0){
		$("#slider-tercero").show(1500);
		$("#tabinserta").show(1500);
	}else{
		$("#slider-tercero").hide(1500);
		$("#tabinserta").hide(1500);
	}
}	
