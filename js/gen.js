function acc(){
	var p_login=$("#login").val();
	var p_pass=$("#pass").val();
	
	$.ajax({
			type: 'POST',
			url: 'ingreso.php',
			data: {login: p_login, pass: p_pass},
			async: true,
			//dataType: "text", 
			success:function(result){
				//alert(result);
				var html='';
				html=result.replace('\n','');
				//alert(html);
				//alert(result+"-"+html);
				if(html=='si'){
					location.reload();
				}else{
					alert('Usuario no existe!!');
				}
			},
			error:function(){
				alert("Error!!")
			}
	});			
}

function seg(pp,pc){
	
	$.ajax({
			type: 'POST',
			url: 'ajaxGen.php',
			data: {o: 1, p: pp, c: pc},
			async: false,
			success:function(result){
			},
			error:function(){
				alert("Error 20143, contacte a un administrador del sitio!!");
			}
	});			
}

function ajaxResumenCot(id_coti){
	$.ajax({
			type: 'POST',
			url: 'AjaxagregaServicios.php',
			data: { op: 9, id_cot: id_coti},
			dataType: 'html',
			async: false,
			success:function(result){
				var html='';
				html=result;
				$("#slider-primero").html(html);
			},
			error:function(){
				alert("Error!!")
			}
	});			
}

function resumenCotMisCompras(id_cot){
	ajaxResumenCot(id_cot);
	$("#slider-primero").show(500);
}

function muestraSidebar(msg, id){
	$("#"+id+"-texto").html("<h4>"+msg+"</h4>");	
	$("#"+id).show(500);	
}

function escondeSidebar(id){
	$("#"+id).hide(500);		
}

function ChangeCSS(Id){
	$(Id).css("border", "1px solid #ffa853" );
	$(Id).css("box-shadow", "0 0 5px 3px #ffa853" );
	$(Id).css("color", "#666" );
	$(Id).css("padding", "2px" );
	$(Id).css("outline", "none" );
	return true;
}

function ValidaRegistro(){
	var ok=true;
	var f=document.getElementById("f_s");
	var ru=$("#txt_rut").val();
	if($.trim($("#txt_rut").val())==''){
		ChangeCSS("#txt_rut");
		$("#lblrut").show(500);
		ok=false;
	}else{
		$.ajax({
				type: 'POST',
				url: 'AjaxGen2.php',
				data: { op: 15, r: ru},
				dataType: 'html',
				async: false,
				success:function(result){
					if(result!="0"){
						$("#lblrut2").show(500);
						ok=false;
					}
				},
				error:function(){
				}
		});
	}
	if($.trim($("#txt_nombre").val())==''){
		ChangeCSS("#txt_nombre");
		$("#lblnombre").show(500);
		ok=false;
	}	
	if($.trim($("#txt_apellido").val())==''){
		ChangeCSS("#txt_apellido");
		$("#lblapellido").show(500);
		ok=false;
	}
	if($.trim($("#txt_email").val())==''){
		ChangeCSS("#txt_email");
		$("#lblemail").show(500);
		ok=false;
	}else{
		if($.trim($("#txt_email").val())!=$.trim($("#txt_email2").val())){
			$("#lblemail2").show(500);
			ok=false;
		}
	}
	if($.trim($("#txt_username").val())==''){
		ChangeCSS("#txt_username");
		$("#lblusername").show(500);
		ok=false;
	}		
	if($.trim($("#id_hotel").val())==''){
		ChangeCSS("#id_hotel");
		$("#lblhotel").show(500);
		ok=false;
	}	
	if($.trim($("#cargo").val())==''){
		ChangeCSS("#cargo");
		$("#lblcargo").show(500);
		ok=false;
	}		

	if(ok){
		f.submit();
	}
}