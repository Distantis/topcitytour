<?php	 	 
@session_start();
//echo $_SESSION['id'];
$donde_estoy="home";
include("htmlhead.php");
?>
<body>
  <div id="main">	
	<div id="site_content">
		<?php	 	 
			include("head.php");
		?>
	  
	  <div id="content">
        <div class="content_item">
		  <h1>Quienes somos</h1>
          <p>
          	Top city tour es una empresa con m&aacute;s de 10 a&ntilde;os de experiencia en servicios tur&iacute;sticos de Chile, tenemos la m&aacute;s grande variedad de tours y servicios complementarios del mercado con los m&aacute;s altos niveles de calidad de la industria <b>(Certificacion ISO 9001)</b>. Adem&aacute;s poseemos veh&iacute;culos americanos y alemanes para ofrecer a nuestros clientes la mayor comodidad que junto a experiementados gu&iacute;as de turismo nos han convertido en la empresa l&iacute;der en Chile.
          </p>
        </div><!--close content_item-->
         <div class="content_item">
		    <div id="sliderFrame">
		        <div id="slider">
		            <a href="#">
		            	<img src="images/productos/casinoenjoy.jpg" style="width: 150px; height:200px;"/>
		        	</a>

		            <a href="#">
		            	<img src="images/productos/balihai.jpg" style="width: 150px; height:200px;"/>
		        	</a>
		            <a href="#">
		            	<img src="images/productos/citytour.jpg" style="width: 150px; height:200px;"/>
		        	</a>		        	
		            <a href="#">
		            	<img src="images/productos/rutadelvino.jpg" style="width: 150px; height:200px;"/>
		        	</a>		        			        	
		        	<a href="#">
		            	<img src="images/productos/tubingfarellones.jpg" style="width: 150px; height:200px;"/>
		        	</a>		    
		        </div>
		        <!--thumbnails-->
		        <div id="thumbs">
		            <div class="thumb">
		                <div class="thumb-content"><p>Casino Enjoy</p></div>
		                <div style="clear:both;"></div>
		            </div>
		            <div class="thumb">
		                <div class="thumb-content"><p>Cena Show Bali-Hai</p></div>
		                <div style="clear:both;"></div>
		            </div>
		            <div class="thumb">
		                <div class="thumb-content"><p>Santiago City-Tour</p></div>
		                <div style="clear:both;"></div>
		            </div>	
		            <div class="thumb">
		                <div class="thumb-content"><p>Ruta del Vino</p></div>
		                <div style="clear:both;"></div>
		            </div>
		            <div class="thumb">
		                <div class="thumb-content"><p>Tubing en Farellones</p></div>
		                <div style="clear:both;"></div>
		            </div>			            		            	            
		        </div>
		        <!--clear above float:left elements. It is required if above #slider is styled as float:left. -->
		        <div style="clear:both;height:0;"></div>
		    </div>	    	
		</div>
	  </div><!--close content-->	    
	  <div class="sidebar_container">  		  
		<div class="sidebar">
          <div class="sidebar_item">
            <h2>Reg&iacute;strate</h2>
            <p>Si deseas ingresar y poder comprar nuestros servicios, env&iacute;anos una solicitud de registro.</p>
		      <a href="registro.php">Registro!!</a>
           </div><!--close sidebar_item--> 
         </div><!--close sidebar-->     		
      </div><!--close sidebar_container-->		
    </div><!--close site_content-->	
	<?php	 	 
		include("footer.php");
	?>
  </div><!--close main-->	
</body>
</html>
